#!/usr/bin/env python3
import click
import os

from src import converters


@click.group()
def group():
    pass


@group.command()
def project():
    pass


@group.command()
@click.argument('converter')
@click.argument('input_file')
@click.argument('output_file')
@click.option('--remove-source', '-rs', is_flag=True)
def convert(*args, **kwargs):
    open(kwargs.get('output_file'), 'w').write(
        converters.collection[kwargs.get('converter')].convert(
            open(kwargs.get('input_file')).read()
    ))
    if kwargs.get('remove_source'):
        os.remove(kwargs.get('input_file'))


@group.command()
@click.argument('converter')
@click.argument('output_file')
def convert_sample(*args, **kwargs):
    try:
        open(kwargs.get('output_file'), 'w').write(
            converters.collection[kwargs.get('converter')].sample
        )
    except Exception as e:
        print(f'Sample is not created: {str(e)}')


if __name__ == '__main__':
    group()