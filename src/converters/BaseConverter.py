from abc import ABC, abstractmethod


class BaseConverter(ABC):

    @staticmethod
    @abstractmethod
    def convert(source:str):
        pass