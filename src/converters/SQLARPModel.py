import yaml
from addict import Dict

from .BaseConverter import BaseConverter


class SQLARPModel(BaseConverter):

    base = '''
from flask_restplus import Model as SerializationModel
from flask_restplus import fields
from sqlalchemy.ext.hybrid import hybrid_property

from src.plugins.sqla import sqla


class {}(sqla.Model):
    __tablename__ = '{}'

    # Basic fields
{}
    # Relationships
{}
    # (De)serialization models
{}
    '''

    sample = '''

Account:
  table_name: accounts
  fields:
    id: int,pk
    username: str,unique
    password: str
    company_id: int,fk('companies.id')
  relationships:
    company: Company,cascade='delete',backref='accounts'
  serialization:
    exclude:
      post: id
      put: id

    '''

    field_types = {
        'int': 'sqla.Integer',
        'str': 'sqla.String',
        'bool': 'sqla.Boolean',
        'json': 'sqla.JSON',
        'datetime': 'sqla.DateTime'
    }
    field_modifiers = {
        'pk': 'primary_key=True',
        'fk': 'sqla.ForeignKey',
        'unique': 'unique=True',
        'nullable': 'nullable=True'
    }
    serialization_types = {
        'int': 'fields.Integer',
        'str': 'fields.String',
        'bool': 'fields.Boolean',
        'json': 'fields.Raw',
        'datetime': 'fields.DateTime'
    }

    @staticmethod
    def convert(source:str):
        # Import model source
        specs = Dict(yaml.load(source)) # type: dict

        # Parse model
        model_name, model_conf = list(specs.items())[0]
        table_name = model_conf.table_name

        # Generate basic fields
        basic_fields = ''
        if model_conf.fields:
            for field_name, field_conf in model_conf.fields.items():
                # Parse type and modifiers
                field_type, *field_modifiers = field_conf.split(',')
                # Translate type
                field_type = SQLARPModel.field_types[field_type]
                # Translate modifiers
                field_modifiers = ', '.join(field_modifiers)
                for k,v in SQLARPModel.field_modifiers.items():
                    field_modifiers = field_modifiers.replace(k,v)
                # Generate row
                row = '    {} = sqla.Column({}, {})\n'.format(
                    field_name, field_type, field_modifiers
                )
                # Add to fields
                basic_fields += row
                
        
        # Generate relationships
        relationships = ''
        if model_conf.relationships:
            for relation_name, relation_conf in model_conf.relationships.items():
                # Parse target relation name and modifiers
                relation_target, *relation_modifiers  = relation_conf.split(',')
                # Translate modifiers
                relation_modifiers = ', '.join(relation_modifiers)
                # Generate row
                row = "    {} = sqla.relationship('{}', {})\n".format(
                    relation_name, relation_target, relation_modifiers
                )
                # Add to relationships
                relationships += row
        
        # Generate (de)serialization models
        serialization_models = ''
        if model_conf.fields:
            # POST
            model = "    model_post = SerializationModel('{}|POST', dict( \n".format(model_name)
            for field_name, field_conf in model_conf.fields.items():
                # Pass field if in exclude list
                if field_name in model_conf.serialization.exclude.post or []:
                    continue
                # Parse type and modifiers
                field_type, *field_modifiers = field_conf.split(',')
                # Generate row
                row = '        {}={}(required=True),\n'.format(
                    field_name, SQLARPModel.serialization_types[field_type]
                )
                # Add to model
                model += row
            model += "    )) \n"
            serialization_models += model

            # PUT
            model = "    model_put = SerializationModel('{}|PUT', dict( \n".format(model_name)
            for field_name, field_conf in model_conf['fields'].items():
                # Pass field if in exclude list
                if field_name in model_conf.serialization.exclude.put or []:
                    continue
                # Parse type and modifiers
                field_type, *field_modifiers = field_conf.split(',')
                # Generate row
                row = '        {}={}(required=False),\n'.format(
                    field_name, SQLARPModel.serialization_types[field_type]
                )
                # Add to model
                model += row
            model += "    )) \n"
            serialization_models += model

            # GET
            model = "    model_get = SerializationModel('{}|GET', dict( \n".format(model_name)
            for field_name, field_conf in model_conf['fields'].items():
                # Pass field if in exclude list
                if field_name in model_conf.serialization.exclude['get']:
                    continue
                # Parse type and modifiers
                field_type, *field_modifiers = field_conf.split(',')
                # Generate row
                row = '        {}={},\n'.format(
                    field_name, SQLARPModel.serialization_types[field_type]
                )
                # Add to model
                model += row
            model += "    )) \n"
            serialization_models += model
        
        # Render
        return SQLARPModel.base.format(
            model_name,
            table_name,
            basic_fields,
            relationships,
            serialization_models
        )
