
from .BaseConverter import BaseConverter
from .SQLARPModel import SQLARPModel


collection = {x.__name__:x for x in BaseConverter.__subclasses__()}