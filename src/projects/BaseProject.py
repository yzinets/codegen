from abc import ABC, abstractmethod


class BaseProject(ABC):

    @staticmethod
    @abstractmethod
    def project_structure():
        pass

    @staticmethod
    @abstractmethod
    def init(source:str):
        pass
